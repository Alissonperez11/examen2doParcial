//Crear una función JavaScript  que reciba un número de cédula y actualice el teléfono de
//ese cliente en la base de datos de clientes y en la base de datos de pedidos.
//Previamente validar que sea un dato numérico. local storage
function actualizarTel() {
  var cedula = document.getElementById("cedula")
  var telefono = document.getElementById("telefono")
  cedula = parseInt(cedula.value);
  if (isNaN(cedula)) {
    alert("El número de cédula ingresado no es un número");
  } else {
    if (!localStorage.getItem("cedula")) {
      ingresarDatos(cedula);
    }

    var persona = JSON.parse(localStorage.getItem("persona"));
    var pedido = JSON.parse(localStorage.getItem("pedido"));

    persona.telefono = telefono.value;
    pedido.telefono = telefono.value;
    localStorage.setItem("persona", JSON.stringify(persona));
    localStorage.setItem("pedido", JSON.stringify(pedido));
  }
}

const ingresarDatos = (cedula) => {
  const persona = {
    nombre: "alisson",
    edad: 20,
    cedula: "1314445488",
    telefono: "",
  };
  const pedido = {
    nombrePedido: "pedido 1",
    cliente: "1314445488",
    fechaPedido: "2020-01-01",
    telefono: "",
  };
  localStorage.setItem("cedula", JSON.stringify(cedula));
  localStorage.setItem("persona", JSON.stringify(persona));
  localStorage.setItem("pedido", JSON.stringify(pedido));
};


//Desarrollar una función en Javascript que permita recorrer los números del 1 al 100
// y contar cuantos múltiplos de 7 existen entre el 1 y el 100.
function contarMultiplos() {
  let contador = 0;
  for (let i = 1; i <= 100; i++) {
    if (i % 7 === 0) {
      contador++;
    }
  }
  return contador;
}

//console.log(contarMultiplos());
